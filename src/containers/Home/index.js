// Copyright (C) 2007-2019, GoodData(R) Corporation. All rights reserved.

import React, { PureComponent } from "react";
import "@gooddata/react-components/styles/css/main.css";
import './Home.css';
import { getMonthFilter } from '../../utils/config';
import GRAPH_CONSTS from '../../utils/constants';

import { ColumnChart } from "@gooddata/react-components";
import Dropdown from './Dropdown';

const grossProfitMeasure = "/gdc/md/xms7ga4tf3g3nzucd8380o2bev8oeknp/obj/6877";

class Home extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      curMonth: 1
    }
  }

  viewBy = {
    visualizationAttribute: {
      displayForm: {
        uri: GRAPH_CONSTS.GRAPH_DATE_ATTRIBUTE_MONTH
      },
      localIdentifier: "a1"
    }
  }

  measures = [
    {
      measure: {
        localIdentifier: "m1",
        definition: {
          measureDefinition: {
            item: {
              uri: grossProfitMeasure
            }
          }
        },
        alias: "$ Gross Profit"
      }
    }
  ]

  render() {
    return (
      <div className="App">
        <h2>
          $ Gross Profit in month 
          <Dropdown 
            curMonth={this.state.curMonth} 
            onChangeDropdown={curMonth => this.setState({ curMonth })}
          /> 
          2016
        </h2>
        <div>
          <ColumnChart
            measures={this.measures}
            filters={getMonthFilter(this.state.curMonth)}
            projectId={process.env.REACT_APP_PROJECT_ID}
          />
        </div>
        <h2>$ Gross Profit - All months</h2>
        <div>
          <ColumnChart
            measures={this.measures}
            viewBy={this.viewBy}
            projectId={process.env.REACT_APP_PROJECT_ID}
          />
        </div>
      </div>
    );
  }
}

export default Home;
